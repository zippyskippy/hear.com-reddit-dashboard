import React from 'react';
import { render } from '@testing-library/react';
import { roundToK, decodeHTML, innerHtml, formatWhen } from './helpers'



test('test innerHtml', () => {
    const { getByText } = render(innerHtml('<div>dog</div>'));
    const word = getByText(/dog/);
    expect(word).toBeInTheDocument();
});

test('test decodeHTML', () => {
    const { getByText } = render(decodeHTML('<div>dog</div>'));
    const word = getByText(/dog/);
    expect(word).toBeInTheDocument();
});


test('test roundToK', () => {
    const str = roundToK(99900);
    expect(str.indexOf('99.9k')).toBeGreaterThan(-1);
});


test('test formatWhen(10 seconds ago)', () => {
    const str = formatWhen(Date.now() - 10 * 1000);
    expect(str.indexOf('seconds ago')).toBeGreaterThan(0);
});


test('test formatWhen(10 minutes ago)', () => {
    const str = formatWhen(Date.now() - 10 * 60 * 1000);
    expect(str.indexOf('minutes ago')).toBeGreaterThan(0);
});


test('test formatWhen(10 hours ago)', () => {
    const str = formatWhen(Date.now() - 10 * 60 * 60 * 1000);
    expect(str.indexOf('hours ago')).toBeGreaterThan(0);
});

test('test formatWhen(10 days ago)', () => {
    const str = formatWhen(Date.now() - 10 * 24 * 60 * 60 * 1000);
    expect(str.indexOf('days ago')).toBeGreaterThan(0);
});

test('test formatWhen(10 years ago)', () => {
    const str = formatWhen(Date.now() - 10 * 365 * 24 * 60 * 60 * 1000);
    expect(str.indexOf('years ago')).toBeGreaterThan(0);
});

