import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import data from './unpopularopinion-dataset.json'

test('renders r/unpopularopinion', () => {
  const { getByText } = render(<App json={data} />);
  const word = getByText(/r\/unpopularopinion/i);
  expect(word).toBeInTheDocument();
});



