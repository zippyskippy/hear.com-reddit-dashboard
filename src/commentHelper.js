
// manipulates the list of comments from reddit
export class commentHelper {

    find(id, comments) {
        return comments.filter(c => { return c.id === id });
    }

    //obviously this ought to udate the datastore too, but that's not an option now
    delete(comment, comments) {
        if (!comments) return [];
        if (!comment) return comments;

        //flag for deletion this comment and any immediate children
        comments.forEach((c, i) => {
            if (c.id === comment.id || c.parent_id === comment.id) c.deleteme = true;
        });

        //now flag any orphaned comments whose parents were flagged above
        comments.forEach((c, i) => {
            if (!c.parent_id) return;
            if (this.find(c.parent_id, comments)[0].deleteme) c.deleteme = true;
        });

        //prune all flagged comments
        return comments.filter(c => { return !c.deleteme });
    }


    //transforms the flat reddit comments list to a sorted hierachial nested array
    sort(comments, ascending) {
        if (!comments) return [];

        const map = {};

        //convert list of comments to a map of all comments
        comments.forEach(e => {
            map[e.id] = Object.assign({}, e); //be sure and clone e
        });

        //add child comments to their parent
        comments.forEach(e => {
            if (e.parent_id) {
                const parent = map[e.parent_id];
                if (parent) {
                    parent.children = parent.children || [];
                    parent.children.push(map[e.id]);  // be sure and use the e clone
                }
            }
        });

        //convert the map to an array of root comments (no parent_id)
        const list = [];
        Object.keys(map).forEach(key => {
            if (!map[key]?.parent_id) list.push(map[key]);
        });

        //sort the children of each node
        list.forEach(e => {
            if (e && e.children) e.children.sort((a, b) => {
                if (ascending) return b.created_utc - a.created_utc;
                else return a.created_utc - b.created_utc;
            });
        });

        //sort the root list
        return list.sort((a, b) => {
            if (ascending) return b.created_utc - a.created_utc;
            else return a.created_utc - b.created_utc;
        });

    }
}

export default new commentHelper(); //singleton