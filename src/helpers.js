import React from 'react';


// converts html text to jsx
export function innerHtml(html) {
    return <span dangerouslySetInnerHTML={{ __html: html }} />
}

// decodes HTML
export function decodeHTML(text) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = text;
    return innerHtml(textArea.value);
}

// converts 9999 to 9.9k
export function roundToK(score) {
    if (score > 1000) return (score / 1000).toFixed(1) + 'k';
    return score;
}

// friendly way of relating time
export function formatWhen(stamp) {
    const minute = 60 * 1000;
    const hour = 60 * minute;
    const day = 24 * hour;
    const year = 365 * day;

    const delta = Math.round(Date.now() - Number.parseInt(stamp));

    if (delta < minute) return delta + " seconds ago";
    if (delta < 2 * hour) return Math.round(delta / minute) + " minutes ago";
    if (delta < 2 * day) return Math.round(delta / hour) + " hours ago";
    if (delta < year) return Math.round(delta / day) + " days ago";
    return Math.round(delta / year) + " years ago";
}

