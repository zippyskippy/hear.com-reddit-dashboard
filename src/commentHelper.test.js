import data from './unpopularopinion-dataset.json'
import commentHelper from './commentHelper';


test('sort compressed', () => {
    const comments = commentHelper.sort(data.comments);
    expect(comments.length).toBeLessThan(data.comments.length);
});

test('sort descending', () => {
    const comments = commentHelper.sort(data.comments);
    expect(comments[0].created_utc).toBeLessThan(comments[1].created_utc);
});

test('sort ascending', () => {
    const comments = commentHelper.sort(data.comments, true);
    expect(comments[0].created_utc).toBeGreaterThan(comments[1].created_utc);
});

test('delete', () => {
    const original = commentHelper.sort(data.comments);
    const comments = commentHelper.delete(data.comments[0], data.comments);  //must be a root element
    const deleted = commentHelper.sort(comments);
    expect(original.length).toBeGreaterThan(deleted.length);
});
