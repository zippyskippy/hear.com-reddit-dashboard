import React, { useState } from 'react';
import commentHelper from './commentHelper';
import { TrashIcon } from './Icons'
import { roundToK, decodeHTML, formatWhen } from './helpers'


/*
    typical comment record from reddit
 
    {
    "created_utc": 1570697457,
    "subreddit_name_prefixed": "r/unpopularopinion",
    "subreddit": "unpopularopinion",
    "depth": 1,
    "permalink": "/r/unpopularopinion/comments/dfqxf8/taking_showers_with_your_socks_on_is_so_much/f35y9gp/",
    "body_html": "&lt;div class=\"md\"&gt;&lt;p&gt;Or the guy who likes sleeping in jeans&lt;/p&gt;\n\n&lt;p&gt;Edit: to everyone commenting, I meant the dude liked to get into bed at night with the duvet on with jeans, not just taking a nap&lt;/p&gt;\n&lt;/div&gt;",
    "downs": 0,
    "body": "Or the guy who likes sleeping in jeans\n\nEdit: to everyone commenting, I meant the dude liked to get into bed at night with the duvet on with jeans, not just taking a nap",
    "author": "ShitOnMyArsehole",
    "id": "t1_f35y9gp",
    "ups": 638,
    "parent_id": "t1_f35nn9e"
  },
*/


function CommentAuthor(props) {
    const comment = props.comment || {};

    return <>
        <div className="author">{comment.author}</div>
        <div className="points">{roundToK(comment.ups - comment.downs)} points - </div>
        <div className="stamp">{formatWhen(comment.created_utc)}</div>
    </>
}

function Comment(props) {
    const comment = props.comment || {};

    const nestedComments = (comment.children || []).map(comment => {
        return <Comment key={comment.id} comment={comment} deleteComment={props.deleteComment} />;
    });


    return <>
        <div className="comment" key={comment.id}>
            <div className="float-right hover" title="delete"
                onClick={() => { props.deleteComment(comment) }}>
                <TrashIcon />
            </div>
            <CommentAuthor comment={comment} />
            <div>{decodeHTML(comment.body_html)} </div>
            {nestedComments}
        </div>
    </>

}

function SortButton(props) {
    return <button
        onClick={props.toggleSort}
    >
        {props.chronological ? "reverse order" : "chronological order"}

    </button>
}

function Comments(props) {

    const [chronological, setChronological] = useState(props.chronological);
    const [comments, setComments] = useState(commentHelper.sort(props.data.comments, chronological));

    const toggleSort = () => {
        setChronological(!chronological);
        setComments(commentHelper.sort(props.data.comments, !chronological));
    }

    const deleteComment = (comment) => {
        props.data.comments = commentHelper.delete(comment, props.data.comments);
        props.setCount(props.data.comments.length);
        setComments(commentHelper.sort(props.data.comments, chronological));
    }


    return (comments)
        ? <>
            {
                (comments.length > 1)
                    ? <div>Comments are in <SortButton toggleSort={toggleSort} chronological={chronological} /></div>
                    : null
            }{
                comments.map(comment => {
                    return <Comment key={comment.id} comment={comment} deleteComment={deleteComment} setCount={props.setCount} />;
                })
            }
        </>
        : "Loading...";

}

export default Comments;  