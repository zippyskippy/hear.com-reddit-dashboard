import config from './config'

// returns live data from url (default url in config)
class Api {
    fetch(url) {
        url = url || config.url;

        return fetch(url)
            .then((response) => {
                return response.json();
            });
    }
}

export default new Api();