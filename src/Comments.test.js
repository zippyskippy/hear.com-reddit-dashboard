import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import data from './unpopularopinion-dataset.json'

/*
The following tests are too specific, fragile, and only valid for the specific data set 
https://gist.githubusercontent.com/mkg0/6a4dca9067ad7a296204e7c9ecd977b0/raw/0b1ec16580ea1e970a73f5c85563c22631be7ad7/unpopularopinion-dataset.json
*/

test('renders a comment with 97%', () => {
    const { getByText } = render(<App json={data} />);
    const word = getByText(/97%/);
    expect(word).toBeInTheDocument();
});

test('renders a comment with Sazzybee', () => {
    const { getByText } = render(<App json={data} />);
    const word = getByText(/Sazzybee/);
    expect(word).toBeInTheDocument();
});
