import React, { useState } from 'react';
import './App.css';
import Comments from './Comments'
import Api from './Api'
import { CommentIcon } from './Icons'
import { roundToK, decodeHTML } from './helpers'


function Header(props) {
  const data = props.data || {};
  return <>
    <div className="header">
      <div className="subreddit_name_prefixed">{data.subreddit_name_prefixed}</div>
      <div className='titleRow'>
        <div className="score">{roundToK(data.score)}</div>
        <div className="title">{data.title}</div>
      </div>
    </div>
  </>
}

function Post(props) {
  const data = props.data || {};

  return <div className="post">
    <div className="selftext">{decodeHTML(data.selftext_html)}</div>
    <div className="commentcount"><CommentIcon /> {props.count} Comments</div>
  </div>
}

function App(props) {
  const [data, setData] = useState(props?.json);
  const [count, setCount] = useState(0);

  if (!data) Api.fetch().then(json => {
    setData(json);
    setCount(json.comments.length);
  });

  return data
    ? <div className="page">
      <Header data={data} />
      <div className="App">
        <Post data={data} count={count} />
        <Comments data={data} setCount={setCount} />
      </div>
    </div>
    : "Loading...";

}

export default App;
