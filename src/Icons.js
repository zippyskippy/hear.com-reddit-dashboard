import React from 'react';

export function CommentIcon(props) {
    const size = props.size || "10px";
    return <svg width={size} height={size} viewBox="0 0 20 20" version="1.1" >
        <g id="Icons" stroke="none" fill="none" >
            <g id="Rounded" transform="translate(-680.000000, -2060.000000)">
                <g id="Editor" transform="translate(100.000000, 1960.000000)">
                    <g id="-Round-/-Editor-/-mode_comment" transform="translate(578.000000, 98.000000)">
                        <g transform="translate(0.000000, 0.000000)">
                            <polygon id="Path" points="0 0 24 0 24 24 0 24"></polygon>
                            <path d="M22,4 C22,2.9 21.1,2 20,2 L4,2 C2.9,2 2,2.9 2,4 L2,16 C2,17.1 2.9,18 4,18 L18,18 L22,22 L22,4 Z" id="🔹-Icon-Color" fill="#1D1D1D"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
}

export function TrashIcon(props) {
    const size = props.size || "15px";
    return <svg width={size} height={size} viewBox="0 0 32 32" version="1.1" >
        <g id="Page-1" stroke="none" fill="none" >
            <g id="icon-27-trash-can" fill="#000000">
                <path d="M23,7 L21,7 L21,7 L21,5.0048815 C21,3.89761602 20.1041422,3 19.0026083,3 L13.9973917,3 C12.8942627,3 12,3.8938998 12,5.0048815 L12,7 L10,7 L6,7 L6,8 L8,8 L8,26.9931517 C8,28.6537881 9.33396149,30 11.0001262,30 L21.9998738,30 C23.6567977,30 25,28.6640085 25,26.9931517 L25,8 L27,8 L27,7 L23,7 L23,7 Z M12,10 L12,27 L13,27 L13,10 L12,10 L12,10 Z M16,10 L16,27 L17,27 L17,10 L16,10 L16,10 Z M20,10 L20,27 L21,27 L21,10 L20,10 L20,10 Z M14.0029293,4 C13.4490268,4 13,4.44266033 13,4.99895656 L13,7 L20,7 L20,4.99895656 C20,4.44724809 19.5621186,4 18.9970707,4 L14.0029293,4 L14.0029293,4 Z" id="trash-can" ></path>
            </g>
        </g>
    </svg >
}